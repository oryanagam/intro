<?php
  include "class.php";
  include "db.php";
  include "query.php";
?>

<html>
  <head>
  <title> Object Oriented PHP </title>
  </head>
  <body>
    <p>
    <?php
    //$text = 'hello world';
     //echo "$text and the universe";
     //echo '<br>';
     //echo $text.' and the universe';
     //echo '<br>';
     //$msg = new Message();
     //echo $msg->text;
     //$msg->show();
     //$msg1 = new Message('A new Text');
     //$msg1->show();
     //$msg1 = new Message();
     //$msg1->show();
     //echo '<br>';
     //echo Message::$count;
     //$msg2 = new Message();
     //$msg2->show();
     //echo '<br>';
     //echo Message::$count;
     //echo '<br>';
     //$msg3 = new redMessage('A Red Message');
     //$msg3->show();
     //echo '<br>';
     //$msg4 = new coloredMessage('A Colored Message');
     //$msg4->color = 'blue';
     //$msg4->show();
     //$msg4->setColor('red');
     //echo '<br>';
     //$msg5 = new coloredMessage('A Colored Message5 ');
     //$msg5->color = 'green';
     //$msg5->show();
     //echo '<br>';
     //showObject($msg5);
     //echo '<br>';
     //showObject($msg1);
     
     $db = new DB('localhost','intro','root','');
     $dbc = $db->connect();
     $query = new Query($dbc);
     $q = "SELECT * FROM users";
     $result = $query->query($q);
     echo '<br>';
     //echo $result->num_rows;
     if($result->num_rows > 0){
       echo '<table>';
       echo '<tr><th>Name</th><th>Email</th></tr>';
       while($row=$result->fetch_assoc() ){
        echo '<tr>';
        echo '<td>'.$row['name'].'</td><td>'.$row['email'].'</td>';
        echo '</tr>';
       }
       echo '</table>';
     }
     else{
       echo "sorry no results";
     }

     ?>
    </p>
  </body>
</html>

